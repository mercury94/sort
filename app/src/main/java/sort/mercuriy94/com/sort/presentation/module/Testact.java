package sort.mercuriy94.com.sort.presentation.module;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.OnLifecycleEvent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by nikit on 15.09.2017.
 */

public class Testact extends AppCompatActivity implements LifecycleObserver  {

    public static final String TAG = "Testact";
    private final LifecycleRegistry mRegistry = new LifecycleRegistry(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         getLifecycle().addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    void create(LifecycleOwner appCompatActivity) {
        Log.d(TAG, "create");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    void start() {
        Log.d(TAG, "start");
    }

    class m implements LifecycleObserver{


    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    void stop() {
        Log.d(TAG, "stop");
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    void destroy() {
        Log.d(TAG, "destroy");

    }

}
