package sort.mercuriy94.com.sort.presentation.common.router.fragmenttransaction;

import android.support.v4.app.FragmentManager;


public abstract class FragmentTransaction {

    public abstract void execute(FragmentManager fragmentManager);

}


