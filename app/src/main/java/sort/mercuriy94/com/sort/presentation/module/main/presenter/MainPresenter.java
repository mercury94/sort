package sort.mercuriy94.com.sort.presentation.module.main.presenter;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;

import sort.mercuriy94.com.sort.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import sort.mercuriy94.com.sort.presentation.model.TitleModel;
import sort.mercuriy94.com.sort.presentation.module.main.MainModuleContract;

/**
 * Created by nikit on 15.09.2017.
 */

@InjectViewState
public class MainPresenter extends MainModuleContract.AbstractMainPresenter {
    public MainPresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
        super(presenterSubcomponentBuilders);
    }

    @Override
    protected MainModuleContract.AbstractMainRouter createRouter() {
        return null;
    }

    @Override
    protected TitleModel getTitle() {
        return null;
    }
}
