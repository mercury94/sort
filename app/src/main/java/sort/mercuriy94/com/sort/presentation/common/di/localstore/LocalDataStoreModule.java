package sort.mercuriy94.com.sort.presentation.common.di.localstore;

import dagger.Module;
import sort.mercuriy94.com.sort.presentation.common.di.repository.LocalRepositoryModule;


/**
 * Created by nikita on 25.12.2016.
 */
@Module(includes = LocalDataStoreModule.Declarations.class)
public class LocalDataStoreModule {

    @Module(includes = LocalRepositoryModule.class)
    public abstract class Declarations {

        //@NonNull
        //@Binds
      //  public abstract LocalDataStore bindLocalDataStore(LocalDataStore localDataStore);

    }
}
