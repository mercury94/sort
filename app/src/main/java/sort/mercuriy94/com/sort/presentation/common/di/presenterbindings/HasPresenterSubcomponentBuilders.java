package sort.mercuriy94.com.sort.presentation.common.di.presenterbindings;


import sort.mercuriy94.com.sort.presentation.common.presenter.BasePresenter;

/**
 * Created by Nikita on 05.05.2017.
 */

public interface HasPresenterSubcomponentBuilders {

    PresenterComponentBuilder getPresenterComponentBuilder(Class<? extends BasePresenter> presenterClass);

}
