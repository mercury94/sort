package sort.mercuriy94.com.sort.presentation.common.di.presenterbindings;



import dagger.MembersInjector;
import sort.mercuriy94.com.sort.presentation.common.presenter.BasePresenter;

/**
 * Created by Nikita on 05.05.2017.
 */

public interface PresenterComponent<Presenter extends BasePresenter> extends MembersInjector<Presenter> {
}
