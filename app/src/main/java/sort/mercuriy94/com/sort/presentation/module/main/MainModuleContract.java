package sort.mercuriy94.com.sort.presentation.module.main;

import android.support.annotation.NonNull;

import sort.mercuriy94.com.sort.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import sort.mercuriy94.com.sort.presentation.common.presenter.BasePresenter;
import sort.mercuriy94.com.sort.presentation.common.router.ActivityRouterAdapter;
import sort.mercuriy94.com.sort.presentation.common.router.BaseActivityRouter;
import sort.mercuriy94.com.sort.presentation.common.view.IBaseView;

/**
 * Created by nikit on 15.09.2017.
 */

public abstract class MainModuleContract {

    public interface IMainView extends IBaseView {

    }

    public abstract static class AbstractMainPresenter extends BasePresenter<IMainView, AbstractMainRouter> {

        public AbstractMainPresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
            super(presenterSubcomponentBuilders);
        }

    }

    public abstract static class AbstractMainRouter extends BaseActivityRouter<ActivityRouterAdapter> {

        public AbstractMainRouter(@NonNull ActivityRouterAdapter routerAdapter) {
            super(routerAdapter);
        }
    }


}
