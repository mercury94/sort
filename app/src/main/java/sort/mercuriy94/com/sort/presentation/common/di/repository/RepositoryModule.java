package sort.mercuriy94.com.sort.presentation.common.di.repository;


import dagger.Module;


/**
 * Created by Nikita on 13.04.2017.
 */

@Module(includes = {LocalRepositoryModule.class})
public abstract class RepositoryModule {


}
