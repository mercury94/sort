package sort.mercuriy94.com.sort.presentation.common.di.presenterbindings;


import dagger.MapKey;
import sort.mercuriy94.com.sort.presentation.common.presenter.BasePresenter;

/**
 * Created by Nikita on 05.05.2017.
 */

@MapKey
public @interface PresenterKey {

    Class<? extends BasePresenter> value();

}
