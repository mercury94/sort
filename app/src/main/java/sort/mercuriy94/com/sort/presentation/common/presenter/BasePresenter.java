package sort.mercuriy94.com.sort.presentation.common.presenter;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.arellomobile.mvp.MvpPresenter;

import sort.mercuriy94.com.sort.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import sort.mercuriy94.com.sort.presentation.common.router.BaseActivityRouter;
import sort.mercuriy94.com.sort.presentation.common.view.IBaseView;
import sort.mercuriy94.com.sort.presentation.model.TitleModel;


/**
 * Created by nikita on 21.01.2017.
 */

public abstract class BasePresenter<View extends IBaseView, Router extends BaseActivityRouter>
        extends MvpPresenter<View> {

    public static final String TAG = "BasePresenter";

    protected Router mRouter;


    public BasePresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
        mRouter = createRouter();
        inject(presenterSubcomponentBuilders);
    }

    public void inject(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {

    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        updateTitle();
    }

    @Override
    public void attachView(View view) {
        super.attachView(view);
        view.setInitialState();
    }

    protected abstract Router createRouter();

    public Router getRouter() {
        return mRouter;
    }

    public boolean onBackPressed() {
        mRouter.onBackPressed();
        return true;
    }

    protected abstract TitleModel getTitle();

    protected void onBindTitle(TitleModel title) {
        if (title == null || getViewState() == null) return;
        if (title.getTitleMessageRes() != 0) {
            getViewState().setTitleText(title.getTitleMessageRes());
        }

        if (!TextUtils.isEmpty(title.getTitleMessage())) {
            getViewState().setTitleText(title.getTitleMessage());
        }

        getViewState().setVisibilityBackButton(title.isVisibleBackButton());
        if (!TextUtils.isEmpty(title.getImageWeb())) {
            getViewState().setTitleImage(title.getImageWeb());
        }
    }

    public void updateTitle() {
        onBindTitle(getTitle());
    }

}
