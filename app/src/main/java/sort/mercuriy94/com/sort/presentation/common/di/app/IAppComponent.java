package sort.mercuriy94.com.sort.presentation.common.di.app;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import sort.mercuriy94.com.sort.presentation.common.di.datastore.DataStoreModule;
import sort.mercuriy94.com.sort.presentation.common.di.presenterbindings.PresenterBindingModule;
import sort.mercuriy94.com.sort.presentation.common.di.rxschedulers.RxSchedulerModule;
import sort.mercuriy94.com.sort.presentation.module.app.App;


@Singleton
@Component(modules = {
        AppModule.class,
        PresenterBindingModule.class,
        DataStoreModule.class,
        RxSchedulerModule.class})

public interface IAppComponent {

    Context context();

    App inject(App application);

}
