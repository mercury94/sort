package sort.mercuriy94.com.sort.presentation.common.router;

import android.app.ActivityOptions;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.Stack;

import sort.mercuriy94.com.sort.presentation.common.router.fragmenttransaction.AddFragmentTransaction;
import sort.mercuriy94.com.sort.presentation.common.router.fragmenttransaction.FragmentTransaction;
import sort.mercuriy94.com.sort.presentation.common.router.fragmenttransaction.RemoveFragmentTransaction;
import sort.mercuriy94.com.sort.presentation.common.router.fragmenttransaction.ReplaceFragmentTransaction;
import sort.mercuriy94.com.sort.presentation.common.router.fragmenttransaction.TransactionRemoveFragmentByTag;
import sort.mercuriy94.com.sort.presentation.common.view.BaseFragment;


/**
 * Created by nikita on 11.03.2017.
 */
public abstract class BaseActivityRouter<Adapter extends ActivityRouterAdapter>
        implements LifecycleObserver {
    public static final String TAG = "BaseActivityRouter";

    @Nullable
    public Adapter mAdapter;
    private boolean mActivityStateLoss = false;
    private Stack<FragmentTransaction> mTransactionStack;

    public BaseActivityRouter(@NonNull Adapter routerAdapter) {
        setAdapter(routerAdapter);
        mTransactionStack = new Stack<>();
    }

    @Nullable
    public Adapter getAdapter() {
        return mAdapter;
    }

    private void setAdapter(@Nullable Adapter adapter) {
        mAdapter = adapter;
   //     if (mAdapter != null) mAdapter.getBaseActivity().getLifecycle().addObserver(this);
    }

    public void createActivity(Lifecycle lifecycle, Context context, FragmentManager fragmentManager){

    }

    public void close() {
   //     if (getAdapter() != null) getAdapter().finishActivity();
    }


    public void navigateToActivity(@NonNull Intent intent) {
        if (getAdapter() != null) getAdapter().navigateToActivity(intent);
    }


    public void navigateToActivity(@NonNull Intent intent, ActivityOptions options) {
        if (getAdapter() != null) getAdapter().navigateToActivity(intent, options);
    }


    protected void addFragment(@NonNull BaseFragment fragment, int container, String tag) {
        executeTransaction(new AddFragmentTransaction(fragment, container, tag));
    }

    protected void replaceFragment(
            @NonNull BaseFragment fragment,
            int container,
            String tag,
            boolean onlyOneInstanceInStack) {
        executeTransaction(new ReplaceFragmentTransaction(fragment, container, tag, onlyOneInstanceInStack));
    }

    private void executeTransaction(FragmentTransaction transaction) {
        if (mActivityStateLoss || (getAdapter() == null)) mTransactionStack.push(transaction);
        else getAdapter().executeFragmentTransaction(transaction);
    }

    @SuppressWarnings("ConstantConditions")
    protected void removeFragment(String tag) {
        executeTransaction(new TransactionRemoveFragmentByTag(tag));
    }


    protected void removeFragment(@NonNull BaseFragment baseFragment) {
        executeTransaction(new RemoveFragmentTransaction(baseFragment));
    }

    public void onBackPressed() {
        if (getAdapter() != null) {
            int count = getAdapter().getBackStackEntryCount();
            if (count == 1 || count == 0) {
              //  getAdapter().finishActivity();
            } else {
                Fragment fragment = getAdapter()
                        .getLastFragmentFromContainer(getAdapter().getContainerForFragmentsId());
                if (fragment != null) removeFragment((BaseFragment) fragment);
            }
        }
    }


    //region LifecycleObserver

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    void onStart() {
        mActivityStateLoss = false;
        if (!mTransactionStack.isEmpty() && mAdapter != null) {
            while (!mTransactionStack.isEmpty()) {
                mAdapter.executeFragmentTransaction(mTransactionStack.pop());
            }
        }
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    void onStop() {
        mActivityStateLoss = true;
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    void onDestroy() {
      //  if (mAdapter != null) mAdapter.getBaseActivity().getLifecycle().removeObserver(this);
    }

    //endregion LifecycleObserver



}
