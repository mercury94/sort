package sort.mercuriy94.com.sort.presentation.module.main.presenter.assembly;

import dagger.Module;
import sort.mercuriy94.com.sort.presentation.common.di.presenterbindings.PresenterModule;
import sort.mercuriy94.com.sort.presentation.module.main.presenter.MainPresenter;

/**
 * Created by nikit on 15.09.2017.
 */

@Module
public class MainPresenterModule extends PresenterModule<MainPresenter> {
    public MainPresenterModule(MainPresenter mainPresenter) {
        super(mainPresenter);
    }
}
