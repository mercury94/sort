package sort.mercuriy94.com.sort.presentation.common.di.presenterbindings;

import dagger.Module;
import dagger.Provides;
import sort.mercuriy94.com.sort.presentation.common.di.scope.PresenterScope;

/**
 * Created by Nikita on 05.05.2017.
 */
@Module
public abstract class PresenterModule<Presenter> {

    protected final Presenter mPresenter;

    public PresenterModule(Presenter presenter) {
        mPresenter = presenter;
    }

    @Provides
    @PresenterScope
    public Presenter providePresenter() {
        return mPresenter;
    }
}
