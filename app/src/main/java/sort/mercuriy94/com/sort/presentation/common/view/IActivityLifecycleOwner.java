package sort.mercuriy94.com.sort.presentation.common.view;

import android.arch.lifecycle.LifecycleRegistryOwner;
import android.content.Context;
import android.support.v4.app.FragmentManager;

/**
 * Created by nikit on 16.09.2017.
 */

public interface IActivityLifecycleOwner extends LifecycleRegistryOwner {

    Context getContext();

    FragmentManager getFragmentsManager();
}
