package sort.mercuriy94.com.sort.presentation.module.main.presenter.assembly;

import dagger.Subcomponent;
import sort.mercuriy94.com.sort.presentation.common.di.presenterbindings.PresenterComponent;
import sort.mercuriy94.com.sort.presentation.common.di.presenterbindings.PresenterComponentBuilder;
import sort.mercuriy94.com.sort.presentation.common.di.scope.PresenterScope;
import sort.mercuriy94.com.sort.presentation.module.main.presenter.MainPresenter;

/**
 * Created by nikit on 15.09.2017.
 */

@PresenterScope
@Subcomponent(modules = MainPresenterModule.class)
public interface IMainPresenterSubcomponent extends PresenterComponent<MainPresenter> {

    @Subcomponent.Builder
    interface Builder extends PresenterComponentBuilder<MainPresenterModule, IMainPresenterSubcomponent> {

    }

}