package sort.mercuriy94.com.sort.presentation.common.view;

/**
 * Created by nikita on 15.03.2017.
 */

public interface IOnBackPressedListener {

    boolean onBackPressed();
}
