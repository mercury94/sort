package sort.mercuriy94.com.sort.presentation.common.router;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import sort.mercuriy94.com.sort.presentation.common.router.fragmenttransaction.FragmentTransaction;
import sort.mercuriy94.com.sort.presentation.common.view.BaseFragment;


public class ActivityRouterAdapter implements FragmentManager.OnBackStackChangedListener {


    private int mContainerForFragmentsId;

    private Context mContext;
    private FragmentManager mFragmentManager;

    public ActivityRouterAdapter(@NonNull Context context, @NonNull FragmentManager manager, int containerForFragmentsId) {
        mContext = context;
        mFragmentManager = manager;
        mContainerForFragmentsId = containerForFragmentsId;
        mFragmentManager.addOnBackStackChangedListener(this);
    }

    public ActivityRouterAdapter(@NonNull Context context, @NonNull FragmentManager manager) {
        this(context, manager, 0);
    }

    public void setOnBackStackChangedListener(@NonNull FragmentManager.OnBackStackChangedListener onBackStackChangedListener) {
        mFragmentManager
                .addOnBackStackChangedListener(onBackStackChangedListener);
    }

    public int getContainerForFragmentsId() {
        return mContainerForFragmentsId;
    }

    @Nullable
    public Fragment getLastFragmentFromContainer(int container) {
        if (container == 0) return null;
        return mFragmentManager
                .findFragmentById(mContainerForFragmentsId);
    }


    public void navigateToActivity(@NonNull Intent intent) {
        mContext.startActivity(intent);
    }

    public void navigateToActivity(@NonNull Intent intent, ActivityOptions options) {
        mContext.startActivity(intent, options.toBundle());
    }


    public void navigateToActivity(@NonNull Class<?> c) {
        mContext.startActivity(new Intent(mContext, c));
    }

    public void executeFragmentTransaction(@NonNull FragmentTransaction fragmentTransaction) {
        fragmentTransaction.execute(mFragmentManager);
    }

    public int getBackStackEntryCount() {
        return mFragmentManager.getBackStackEntryCount();
    }

    //region OnBackStackChangedListener

    @Override
    public void onBackStackChanged() {
        Fragment fragment = getLastFragmentFromContainer(mContainerForFragmentsId);
        if ((fragment != null) && (fragment instanceof BaseFragment)) {
            ((BaseFragment) fragment).onBackStackChanged((BaseFragment) fragment);
        }
    }

    //endregion OnBackStackChangedListener
}
