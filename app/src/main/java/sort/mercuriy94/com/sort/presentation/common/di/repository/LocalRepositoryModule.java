package sort.mercuriy94.com.sort.presentation.common.di.repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import sort.mercuriy94.com.sort.presentation.common.di.objectbox.ObjectBoxModule;


/**
 * Created by nikita on 27.12.2016.
 */

@Module(includes = {ObjectBoxModule.class, LocalRepositoryModule.Declarations.class,})
public class LocalRepositoryModule {

    @NonNull
    @Provides
    public SharedPreferences provideSharedPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }


    @Module
    public abstract class Declarations {


    }

}
