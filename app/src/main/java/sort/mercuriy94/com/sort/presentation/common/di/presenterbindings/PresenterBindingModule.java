package sort.mercuriy94.com.sort.presentation.common.di.presenterbindings;


import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import sort.mercuriy94.com.sort.presentation.module.main.presenter.MainPresenter;
import sort.mercuriy94.com.sort.presentation.module.main.presenter.assembly.IMainPresenterSubcomponent;


/**
 * Created by Nikita on 05.05.2017.
 */

@Module(subcomponents = {IMainPresenterSubcomponent.class})
public abstract class PresenterBindingModule {

    @Binds
    @IntoMap
    @PresenterKey(MainPresenter.class)
    public abstract PresenterComponentBuilder bindMainPresenterSubomponent(IMainPresenterSubcomponent.Builder impl);

}
