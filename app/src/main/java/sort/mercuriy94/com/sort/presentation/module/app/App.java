package sort.mercuriy94.com.sort.presentation.module.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

import sort.mercuriy94.com.sort.presentation.common.di.app.AppModule;
import sort.mercuriy94.com.sort.presentation.common.di.app.DaggerIAppComponent;
import sort.mercuriy94.com.sort.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import sort.mercuriy94.com.sort.presentation.common.di.presenterbindings.PresenterComponentBuilder;
import sort.mercuriy94.com.sort.presentation.common.presenter.BasePresenter;

public class App extends Application implements HasPresenterSubcomponentBuilders, Application.ActivityLifecycleCallbacks {

    public static final String TAG = "App";

    @Inject
    Map<Class<? extends BasePresenter>, Provider<PresenterComponentBuilder>> mPresenterComponentBuilders;

    @Override
    public void onCreate() {
        super.onCreate();
        inject(this);
        registerActivityLifecycleCallbacks(this);
    }

    public static HasPresenterSubcomponentBuilders getHasPresenterSubcomponentBuilders(Context context) {
        return ((HasPresenterSubcomponentBuilders) context.getApplicationContext());
    }


    public void inject(Context context) {
        DaggerIAppComponent.builder()
                .appModule(new AppModule(context))
               // .localRepositoryModule(new LocalRepositoryModule())
                //.objectBoxModule(new ObjectBoxModule())
                //.rxSchedulerModule(new RxSchedulerModule())
                .build()
                .inject(this);
    }


    @Override
    public PresenterComponentBuilder getPresenterComponentBuilder(Class<? extends BasePresenter> presenterClass) {
        return mPresenterComponentBuilders.get(presenterClass).get();
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        int i = 0;
        i++;
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
