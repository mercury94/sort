package sort.mercuriy94.com.sort.presentation.common.app;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import sort.mercuriy94.com.sort.presentation.common.view.ViperBaseActivity;

/**
 * Created by nikit on 16.09.2017.
 */

public class ViperApp extends Application implements Application.ActivityLifecycleCallbacks {


    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        if (activity instanceof ViperBaseActivity) {
            ViperBaseActivity viperBaseActivity = (ViperBaseActivity) activity;
            if (viperBaseActivity.getPresenter().getRouter() == null) {
                //noinspection unchecked
               // viperBaseActivity.getPresenter().setRouter(viperBaseActivity.createRouter());
            }

        }
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
