package sort.mercuriy94.com.sort.presentation.module.main.view;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import sort.mercuriy94.com.sort.presentation.common.view.ViperBaseActivity;
import sort.mercuriy94.com.sort.presentation.module.app.App;
import sort.mercuriy94.com.sort.presentation.module.main.MainModuleContract;
import sort.mercuriy94.com.sort.presentation.module.main.presenter.MainPresenter;

/**
 * Created by nikit on 15.09.2017.
 */

public class MainViewActivity extends ViperBaseActivity<MainModuleContract.AbstractMainPresenter>
        implements MainModuleContract.IMainView {

    @InjectPresenter
    MainModuleContract.AbstractMainPresenter mPresneter;

    @ProvidePresenter
    MainModuleContract.AbstractMainPresenter providePresenter() {
        return new MainPresenter(App.getHasPresenterSubcomponentBuilders(this));
    }

    @Override
    public void initialState() {

    }

    @NonNull
    @Override
    public MainModuleContract.AbstractMainPresenter getPresenter() {
        return mPresneter;
    }


   
}
