package sort.mercuriy94.com.sort.presentation.common.di.datastore;



import dagger.Module;
import sort.mercuriy94.com.sort.presentation.common.di.repository.RepositoryModule;


@Module(includes = RepositoryModule.class)
public  abstract class DataStoreModule {

}
